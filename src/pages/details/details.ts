import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import {Http, Headers,Response} from '@angular/http';
import { HttpClient } from '../services/HttpClient';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { s } from '@angular/core/src/render3';
import { UserDetailsPage } from '../user-details/user-details';

@Component({
  selector: 'page-home',
  templateUrl: 'details.html',
  providers: [HttpClient]
})
export class DetailsPage {
users:any;
  constructor(public navCtrl: NavController,public  http: Http,public  httpClint: HttpClient,public storage: Storage) {
    this.getUsers();
  }
getUsers(){
  var self=this;
  this.httpClint.showLoading();
  this.httpClint.get("api/?results=50").map((res: Response) => res.json()).subscribe(function(res){
    self.httpClint.dismissLoading();
    self.users=res.results;
  }
  , error => {self.httpClint.dismissLoading();self.httpClint.showToast("Oops! Somthing went wrong!");} );
  }
  openDetails(userData){
    this.navCtrl.push(UserDetailsPage,{"user":userData});
  }
}
