import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from 'ionic-angular';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
@Injectable()
export class HttpClient {
  public serviceBase: any;
  loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
  constructor(private http: Http, public storage: Storage, private toastCtrl: ToastController, public loadingCtrl: LoadingController) {
    this.serviceBase = 'https://randomuser.me/';
  }
  showToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  get(url) {
    return this.http.get(this.serviceBase + url);
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: 'Please wait...'
    });
    this.loading.present();
  }
  dismissLoading() {
    if (this.loading)
      this.loading.dismiss();
  }
}