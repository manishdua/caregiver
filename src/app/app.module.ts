import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {IonicStorageModule} from "@ionic/storage";
import {HttpModule} from "@angular/http";
import {HttpClientModule} from "@angular/common/http";
import { HttpClient } from '../pages/services/HttpClient';
import {Services} from "../providers/Services";
import {Helper} from "../providers/helper";
import { DetailsPage } from '../pages/details/details';
import { UserDetailsPage } from '../pages/user-details/user-details';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
		DetailsPage,
    UserDetailsPage
  ],
  imports: [
    HttpModule,
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
		DetailsPage,
    UserDetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Services,
		HttpClient,
    Helper,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
